# Extension for Python KiteConnect API from Zerodha

## Installation

* `pip install git+https://gitlab.com/algo2t/kiteext.git`


## Usage

```python

# Check config.py example

import config
from kiteext import KiteExt

kite = KiteExt()

kite.login_with_credentials(userid=config.username, password=config.password, pin=config.pin)

# Store token to enctoken.txt

with open('enctoken.txt', 'w') as wr:
    wr.write(kite.enctoken)

```

## `config.py` example

```python

username='YourUsernameAB1234'
password='YourSecretPassword'
pin='UrPin123456'

try:
    enctoken = open('enctoken.txt', 'r').read().rstrip()
except Exception as e:
    print('Exception occurred :: {}'.format(e))
    enctoken = None

```

## Usage existing `enctoken.txt`

```python
import config
from kiteext import KiteExt

# providing userid is must either in KiteExt or in set_headers method
kite = KiteExt(userid=config.username)
kite.set_headers(config.enctoken)

```

```python

# providing userid is must either in KiteExt or in set_headers method
kite = KiteExt()
kite.set_headers(config.enctoken, userid=config.username)

```

## Ticker

* Once `kite` object is created then creating ticker is very easy

```python

kws = kite.ticker()

```

